# -*- coding: utf-8 -*-
# @Time    : 2020/6/1
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : subdomanScan.py


from .. import models


def subdomain_to_asset(asset_subdomain):
    for item in asset_subdomain:
        asset_get = models.Asset.objects.filter(key=item)
        if not asset_get:
            models.Asset.objects.get_or_create(name=item, key=item)
    return True
