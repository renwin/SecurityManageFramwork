# coding:utf-8

from django.contrib import admin
from . import models
# Register your models here.

admin.site.register(models.Building_Event)
admin.site.register(models.Building_Event_Action)
admin.site.register(models.Building_Event_LEVEL)

admin.site.register(models.Building_Projects)
admin.site.register(models.Building_Projects_LEVEL)
admin.site.register(models.Building_Projects_Action)

admin.site.register(models.Building_Phishing)
admin.site.register(models.Building_Phishing_Victim)
