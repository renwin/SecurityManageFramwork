# -*- coding:utf-8 -*-
# @Time: 2020/12/31
# @Author: 残源
# @Mail: canyuan@semf.top
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from Base.Functions.basefun import xssfilter
from Workflow.Functions import ticketfun
from ..Functions import sharefun
from rest_framework.permissions import AllowAny
from .. import models, serializers


@api_view(['GET'])
def ticket2share(request, ticket_id):
    data = {
        "code": 1,
        "msg": "",
        "data": ""
    }
    user = request.user
    item_get = ticketfun.check_user_permission(user, ticket_id)
    if item_get:
        share_key = sharefun.vuln2share(item_get,user)
        if share_key:
            data['code'] = 0
            data['data'] = share_key
            # data['data'] = "/workflow/share/view/{}/".format(share_key)
            data['msg'] = 'success'
        else:
            data['msg'] = '分享失败，联系管理员'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def deny2share(request, ticket_id):
    data = {
        "code": 1,
        "msg": "",
        "data": ""
    }
    user = request.user
    item_get = ticketfun.check_user_permission(user, ticket_id)
    if item_get:
        share_item = models.ShareKey.objects.filter(ticket=item_get).all()
        share_item.update(is_use=False)
        item_get.is_share = False
        item_get.save()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def share_details(request, key):
    data = {
        "code": 1,
        "msg": "",
        "data": "",
    }
    shareitem = sharefun.share_check(key)
    if shareitem:
        data['data'] = xssfilter(ticketfun.ticket_details(shareitem.ticket))
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)