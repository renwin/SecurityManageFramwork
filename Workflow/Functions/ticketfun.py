# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : checkfun.py

from .. import models, serializers
from Task.models import Task
from Vuln.models import Vuln
from . import logfun
from Task.serializers import TaskDetailsSerializer
from Vuln.serializers import VulnDetailsSerializer
from Scanner.Functions import timesetfun


# 检查当前用户是否有该流程单的权限
def check_user_permission(user_obj, ticket_id):
    if user_obj.is_superuser:
        item_get = models.Tickets.objects.filter(id=ticket_id).first()
        if item_get:
            return item_get
    return None


# 获取当前用户下特定流程下的所有单据
def list_workflow(user_obj, workflow_key):
    if user_obj.is_superuser:
        ticket_list = models.Tickets.objects.filter(workflow__key=workflow_key)
        return ticket_list
    return None


def list_ticket(user_obj, ticket_key):
    if user_obj.is_superuser:
        ticket_list = models.Tickets.objects.filter(ticket_key=ticket_key)
        return ticket_list
    return None


# 同步变更流程单对应的数据主体的状态
def workflowaction(ticket_obj):
    ticket_key = ticket_obj.ticket_key
    workflow_get = ticket_obj.workflow
    if ticket_key == 'task':
        task_get = Task.objects.filter(id=ticket_obj.ticket_id).first()
        if task_get:
            task_get.status = workflow_get.status
            task_get.save()
    elif ticket_key == 'vuln':
        vuln_get = Vuln.objects.filter(id=ticket_obj.ticket_id).first()
        if vuln_get:
            vuln_get.status = workflow_get.status
            vuln_get.save()
    else:
        return False
    return True


# 执行流程审批操作前触发的行为
def workflow_check_before(ticket_obj, workflow_next):
    #workflow_get = ticket_obj.workflow
    # 这个写各个流程对应的操作
    if workflow_next.key == 'vuln_fix':
        vuln_get = Vuln.objects.filter(id=ticket_obj.ticket_id).first()
        if vuln_get:
            vuln_get.is_check = True
            vuln_get.save()
        else:
            return False
    elif workflow_next.key == 'task_deal':
        task_get = Task.objects.filter(id=ticket_obj.ticket_id).first()
        if task_get:
            timesetfun.task_plan_create(task_get)
        else:
            return False
    else:
        pass
    return True


# 流程单的具体操作
def workflow_flow(ticket_obj, user, description, action=False):
    workflow_next = ticket_obj.workflow.workflow_to_workflow.filter(is_check=action).first()
    if workflow_next:
        res = workflow_check_before(ticket_obj, workflow_next)
        if res:
            ticket_obj.workflow = workflow_next
            ticket_obj.save()  # 修改流程单的状态
            workflowaction(ticket_obj)  # 修改流程单对应数据的状态
            log_get = {
                'user': user,
                'ticket': ticket_obj,
                'description': user.username + '修改流程,备注为：' + description
            }
            logfun.create_workflowlog(log_get)  # 记录日志
            return True
    return False


def ticket_details(ticket_obj):
    data_details = {
        'key': '',
        'is_share': ticket_obj.is_share,
        'info': '',
        'log': ''
    }
    ticket_key = ticket_obj.ticket_key
    if ticket_key == 'task':
        task_get = Task.objects.filter(id=ticket_obj.ticket_id).first()
        if task_get:
            data_details['info'] = TaskDetailsSerializer(task_get).data
    elif ticket_key == 'vuln':
        vuln_get = Vuln.objects.filter(id=ticket_obj.ticket_id).first()
        if vuln_get:
            data_details['info'] = VulnDetailsSerializer(vuln_get).data
    else:
        pass
    log_list = ticket_obj.ticket_for_workflowLog.all()
    data_details['key'] = ticket_key
    data_details['log'] = serializers.WorkflowLogSerializer(log_list, many=True).data
    return data_details
