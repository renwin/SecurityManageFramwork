# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : vulnviews.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import serializers, forms, models
from ..Functions import vulnfun
from django.db.models import Q
from Asset import models as assetmodels


@api_view(['GET'])
def list_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = vulnfun.get_vuln_list(request.user).filter(
        Q(name__icontains=key) |
        Q(type__name__icontains=key) |
        Q(level__name__icontains=key)
    ).order_by('-update_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.VulnSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def delete_views(request, vuln_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = vulnfun.check_vuln_permission(vuln_id, request.user)
    if item_get:
        item_get.delete()
        data['code'] = 0
        data['msg'] = 'success'

    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    form = forms.VulnForm(request.POST)
    if form.is_valid():
        data_vuln = {
            'name': form.cleaned_data['name'],
            'type': form.cleaned_data['type'],
            'level': form.cleaned_data['level'],
            'introduce': form.cleaned_data['introduce'],
            'info': form.cleaned_data['info'],
            'fix': form.cleaned_data['fix'],
            'source': models.Source.objects.filter(key='user').first()
        }
        if form.cleaned_data['asset_get']:
            asset_list = assetmodels.Asset.objects.filter(
                id__in=form.cleaned_data['asset_get'].split(',')
            )
            for item in asset_list:
                data_vuln['asset'] = item
                vulnfun.vuln_create(data_vuln, request.user, True)
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '缺少必要参数'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)
