# -*- coding: utf-8 -*-
# @Time    : 2020/11/23
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py
from rest_framework import serializers
from . import models


class TypeSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type
        fields = ('id', 'name')


class SynckeyListSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField('get_type')
    user = serializers.SerializerMethodField('get_user')
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.SyncKey
        fields = ('id', 'type', 'description', 'is_active', 'create_time', 'update_time', 'user')

    def get_type(self, obj):
        try:
            return {'id': obj.type.id, 'name': obj.type.name}
        except:
            return None

    def get_user(self, obj):
        try:
            return obj.user.profile.title
        except:
            return None
