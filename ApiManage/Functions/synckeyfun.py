# -*- coding: utf-8 -*-
# @Time    : 2020/11/19
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : synckeyfun.py

from Base.Functions import syncfun
from Asset.Functions import assetfun
from Vuln.Functions import vulnfun


def key2action(json_data, key_obj):
    if key_obj.type.key == 'vuln':
        for item in json_data:
            vulnfun.dic2vuln(item, key_obj.user)
        return True
    elif key_obj.type.key == 'asset':
        for item in json_data:
            assetfun.dict2asset(item)
        return True
    elif key_obj.type.key == 'person':
        for item in json_data:
            syncfun.dic2person(item)
        return True
    elif key_obj.type.key == 'client':
        for item in json_data:
            syncfun.dic2client(item)
        return True
    elif key_obj.type.key == 'department':
        for item in json_data:
            syncfun.dic2dep(item)
        return True
    else:
        pass
    return False

