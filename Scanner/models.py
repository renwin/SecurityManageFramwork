# coding:utf-8
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
SCANNER_TYPE = (
    ('X-Ray', 'X-Ray'),
    ('Nmap', 'Nmap'),
    ('DnsPod', 'DnsPod'),
)


class Scanner(models.Model):
    name = models.CharField('节点名称', max_length=50)
    type = models.CharField('节点类型', max_length=50, choices=SCANNER_TYPE)
    url = models.URLField('节点地址')
    status = models.BooleanField('是否启用', default=True)
    apikey = models.CharField('API_KEY', max_length=100)
    apisec = models.CharField('API_SEC', max_length=100, null=True, blank=True)
    engine = models.CharField('扫描引擎', null=True, max_length=100)

    description = models.TextField('节点描述')
    addtime = models.DateTimeField('添加时间', auto_now_add=True)
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Scanner'
        verbose_name_plural = '扫描节点'


class Policies(models.Model):
    name = models.CharField('策略名称', max_length=50, help_text='扫描策略为扫描器策略名称')
    key = models.CharField('策略编号', max_length=50, null=True)
    description = models.TextField('策略描述', null=True, blank=True)
    scanner = models.ForeignKey(Scanner, verbose_name='节点关联', related_name='police_for_scanner',
                                on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Policies'
        verbose_name_plural = '扫描策略'
