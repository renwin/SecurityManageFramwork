# -*- coding: utf-8 -*-
# @Time    : 2020/7/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : xrayfun.py

from ... import models
from Vuln.Functions import vulnfun
from Base.Functions import basefun
from urllib.parse import urlparse
import urllib3
import json
import time


http = urllib3.PoolManager(cert_reqs='CERT_NONE')


class X_Ray:
    def __init__(self, scanner_obj):
        self.token = scanner_obj.apikey
        self.url = scanner_obj.url
        self.engine = scanner_obj.engine
        self.headers = {
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'Content-Type': 'application/json',
            'token': self.token
        }

    def check_polices(self, police_id):
        path = self.url + '/api/v2/template/{0}/'.format(police_id)
        urllib3.disable_warnings()
        r = http.request('GET', path, headers=self.headers)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('data'):
            return True
        return False

    def get_polices_list(self):
        path = self.url + '/api/v2/template/?limit=10&offset=0'
        urllib3.disable_warnings()
        r = http.request('GET', path, headers=self.headers)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('data'):
            return data_get.get('data').get('content')
        return False

    def add(self, target, police_id):
        path = self.url + "/api/v2/plan/create/"
        urllib3.disable_warnings()
        data = {
            "active": True,
            "basic_setting": {
                "remark": str(time.time()),
                "taskName": 'target',
                "whiteList": [],
                "taskTarget": {"target": target, "targetType": "MANUAL"},
                "planSetting": {"enabled": False, "planType": "NOW"},
                "engineChoice": [self.engine]
            },
            "task_template_id": police_id
        }
        data = json.dumps(data).encode('utf-8')
        r = http.request('POST', path, headers=self.headers, body=data)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('data'):
            return data_get.get('data').get('id')
        return False

    def stop_scan(self, task_id):
        path = self.url + '/api/v2/xprocess/stop/'
        urllib3.disable_warnings()
        data = {
            "id": task_id
        }
        data = json.dumps(data).encode('utf-8')
        r = http.request('POST', path, headers=self.headers, body=data)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('ok'):
            return True
        return False

    def check_status(self, task_id):
        path = self.url + '/api/v2/xprocess/{0}/'.format(task_id)
        urllib3.disable_warnings()
        r = http.request('GET', path, headers=self.headers)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('data'):
            return data_get.get('data').get('status')
        return False

    def get_xprocess(self, task_id):
        data = {
            "plan_id": task_id,
            'status':'FINISHED'
        }
        path = self.url + '/api/v2/xprocess/filter/'
        urllib3.disable_warnings()
        data = json.dumps(data).encode('utf-8')
        r = http.request('POST', path, headers=self.headers, body=data)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('data'):
            try:
                return data_get.get('data').get('result')[0].get('id')
            except:
                print(data_get)
        return False

    def get_results(self, task_id):
        data = {
            "limit": 0,
            "offset": 0,
            "origin": [
                "string"
            ],
            "xprocess_id": task_id,
            "result_type": "vuln"
        }
        path = self.url + '/api/v2/result/filter/'
        urllib3.disable_warnings()
        data = json.dumps(data).encode('utf-8')
        r = http.request('POST', path, headers=self.headers, body=data)
        data_get = json.loads(r.data.decode('utf-8'))
        if data_get.get('data'):
            return data_get.get('data').get('result')
        return None


def create_scan(scantask_obj):
    scanner = X_Ray(scantask_obj.police.scanner)
    check_res = scanner.check_polices(scantask_obj.police.key)
    if check_res:
        target = []
        for item in scantask_obj.task.asset.all():
            if scantask_obj.type == 'ip':
                if basefun.checkip(item.key):
                    target.append(item.key)
            elif scantask_obj.type == 'url':
                if basefun.checkurl(item.key):
                    target.append(item.key)
            else:
                target.append(item.key)
        scan_id = scanner.add(target, scantask_obj.police.key)
        if scan_id:
            scantask_obj.scan_id = scan_id
            scantask_obj.save()
            time.sleep(10)
            return True
    return False


def stop_scan(scantask_obj):
    scanner = X_Ray(scantask_obj.police.scanner)
    res = scanner.stop_scan(scantask_obj.scan_id)
    if res:
        scantask_obj.status = '已暂停'
        scantask_obj.save()
    return res


def chekc_status(scantask_obj):
    scanner = X_Ray(scantask_obj.police.scanner)
    status = scanner.check_status(scantask_obj.scan_id)
    if status == 'FINISHED':
        scantask_obj.status = status
        scantask_obj.save()
        return True
    else:
        return False


def get_scan_res(scantask_obj):
    scanner = X_Ray(scantask_obj.police.scanner)
    xprocess_id = scanner.get_xprocess(scantask_obj.scan_id)
    if xprocess_id:
        vuln_list = scanner.get_results(xprocess_id)
        if vuln_list:
            for item in vuln_list:
                url_get = urlparse(item.get('target').get('url'))
                if url_get.scheme:
                    key_get = url_get.netloc
                else:
                    key_get = item.get('url')
                vuln_info = {
                    'name': item.get('title'),
                    'info': item.get('summary'),
                    'scope': item.get('target').get('url'),
                    'impact': item.get('impact'),
                    'description': item.get('detail'),
                    'fix': item.get('solution'),
                    'level': item.get('severity'),
                    'source': 'xray',
                    'task': scantask_obj.task,
                    'asset': key_get,
                }
                try:
                    vulnfun.dic2vuln(vuln_info, scantask_obj.user)
                except:
                    vuln_info['description'] = item.get('detail').encode('utf8')
                    vulnfun.dic2vuln(vuln_info, scantask_obj.user)
        return True
    return False


def get_scanner_policies(scanner_obj):
    scanner = X_Ray(scanner_obj)
    policies_list = scanner.get_polices_list()
    for item in policies_list:
        models.Policies.objects.get_or_create(
            name=item.get('name'),
            key=item.get('id'),
            description=item.get('remark'),
            scanner=scanner_obj
        )
    return True


