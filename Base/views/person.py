# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : person.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from django.db.models import Q
from django.views.decorators.csrf import csrf_protect
from ..Functions import dep2tree


@api_view(['GET'])
def person_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        dep = request.GET.get('dep', '0')
        if dep == '0':
            list_get = models.Person.objects.filter(Q(name__icontains=key) | Q(mail__icontains=key)).order_by('id')
        else:
            dep_list = models.Department.objects.filter(id=dep)
            dep_id_list = dep2tree.dep2tree(dep_list)
            list_get = models.Person.objects.filter(Q(name__icontains=key) | Q(mail__icontains=key),
                                                    dep__id__in=dep_id_list).order_by('id')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.PersonSerializer(instance=list_page, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)


@api_view(['GET'])
def person_select(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key')
    if not key:
        key = ''
    list_get = models.Person.objects.filter(Q(name__icontains=key) | Q(mail__icontains=key)).order_by('id')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.PersonSelectSerializer(instance=list_page, many=True)
    data['code'] = 0
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def person_delete(request, person_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = models.Person.objects.filter(id=person_id).first()
        if item_get:
            item_get.delete()
            data['code'] = 0
            data['msg'] = '操作成功'
        else:
            data['msg'] = '你要干啥'
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def person_create(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    form = forms.PersonForm(request.POST)
    if form.is_valid:
        form.save()
        data['code'] = 0
        data['msg'] = '添加成功'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def person_update(request, person_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    if user:
        person_get = models.Person.objects.filter(id=person_id).first()
        if person_get:
            form = forms.PersonForm(request.POST, instance=person_get)
            if form.is_valid:
                form.save()
                data['code'] = 0
                data['msg'] = 'success'
            else:
                data['msg'] = '请检查参数'
        else:
            data['msg'] = '指定参数不存在'
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)
