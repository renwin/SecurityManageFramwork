# -*- coding: utf-8 -*-
# @Time    : 2020/6/2
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : getuserip.py


def get_client_ip(request):
    if request.META.get('HTTP_X_FORWARDED_FOR'):
        ip = request.META['HTTP_X_FORWARDED_FOR']
    else:
        ip = request.META['REMOTE_ADDR']
    return ip
