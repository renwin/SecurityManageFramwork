# -*- coding: utf-8 -*-
# @Time    : 2020/6/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : dep2tree.py

from .. import models


def dep2tree(dep_list):
    dep_id_list = []
    for item in dep_list:
        if item.dep_to_dep.all():
            dep_id_list = dep_id_list + dep2tree(item.dep_to_dep.all())
        dep_id_list.append(item.id)
    return dep_id_list

